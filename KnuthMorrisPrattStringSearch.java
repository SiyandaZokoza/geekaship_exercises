import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-08.
 */
public class KnuthMorrisPrattStringSearch {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter string to search: ");
        String str = scan.nextLine();

        System.out.print("Enter pattern you want to search from string entered above: ");
        String pat = scan.nextLine();

        PatternSearch(str, pat);


    }
    public static void PatternSearch(String input,String pattern)
    {

        String output = "";
        int firstIndex;
        int lastIndex;

        int different = input.length() - pattern.length();

        if(pattern.length() >= input.length())
        {
            output = "Your pattern's length must be less than given String's length";
        }
        else
        {
            for(int x= 0;x <= different;x++)
            {
                if(pattern.equalsIgnoreCase(input.substring(x,pattern.length() + x)))
                {
                    firstIndex = x;
                    lastIndex = pattern.length() + x ;
                    System.out.println(input);
                    System.out.println(pattern );
                    output = "pattern found from index " + firstIndex + " to " + lastIndex;
                    x = different;
                }
                else
                {
                    output = "pattern not found";
                }

            }

        }

        System.out.println(output);

    }
}
