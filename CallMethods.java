/**
 * Created by CHARLIE on 2015-09-18.
 */
public class CallMethods {
    public static void main(String[] args){

        int passingByValue = 3;
        int number = ReturnValue();
        int variableNumber1 = 3, variableNumber2 = 5;
        String fixedArg1 = "This method requires", fixedArg2 = "a fixed number of arguments";
        String optArg1 = "This method requires optional arguments", optArg2 = "This optional argument will not be displayed";

        NoArguments();
        FixedNumberOfArguments(fixedArg1, fixedArg2);
        OptionalArguments(optArg1, optArg2);
        VariableNumberOfArgs(variableNumber1,variableNumber2);
        System.out.println("This method returns the value: " + number);
        ByValue(passingByValue);
        PrivateMethod();
        ProtectedMethod();
        PublicMethod();


    }

    static void FixedNumberOfArguments(String value1, String value2){
        System.out.println(value1 + " " + value2); //I assumed that fixed arguments means all the arguments should be used

    }

    static void OptionalArguments(String value1, String value2)
    {
        System.out.println(value1); //I assumed that optional arguments means not all the arguments need be used
    }

    static void NoArguments(){
        System.out.println("This method requires no arguments");
    }

    static void VariableNumberOfArgs(int value1, int value2){
        System.out.println("This method has a variable number of arguments - Numbers " + value1 + " & " + value2 + " have been passed");
    }

    static int ReturnValue(){
        int number = 5;
        return number;
    }

    static void ByValue(int value)
    {
        System.out.println("The number: " + value + " has been passed by value");
    }

    private static void PrivateMethod()
    {
        System.out.println("This is a Private Method");
    }

    protected  static  void ProtectedMethod()
    {
        System.out.println("This is a Protected Method");
    }

    public  static void PublicMethod()
    {
        System.out.println("This is a Public Method");
    }
}
