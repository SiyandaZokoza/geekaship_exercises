/**
 * Created by CHARLIE on 2015-09-17.
 */
import com.sun.deploy.util.StringUtils;
public class AlignColumns {
    public static void main(String[] args) {
        String[] inputStrings = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line$",
                "are$delineated$by$a$single$'dollar'$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each$",
                "column$are$separated$by$at$least$one$space.",
                "Further,$allow$for$each$word$in$a$column$to$be$either$left$",
                "justified,$right$justified,$or$center$justified$within$its$column."};

        String[] words = null;

        System.out.println("Left Justified Columns:\n");
        for (String aligned : inputStrings) {

            words = aligned.split("\\$");
            for (String eachWord : words){
                System.out.printf("%-11s", eachWord);
            }
            System.out.println();

        }

        System.out.println("\nRight Justified Columns:\n");
        for (String aligned : inputStrings) {

            words = aligned.split("\\$");
            for (String eachWord : words){
                System.out.printf("%11s", eachWord);
            }
            System.out.println();

        }



        System.out.println("\nCentre Justified Columns:\n");

        for (String inputString : inputStrings) {
            words = inputString.split("\\$");
            for (String aligned : words) {


                int width = 11;
                int padsize = width - aligned.length();
                int padstart = aligned.length() + padsize / 2;

                aligned = String.format("%" + padstart + "s", aligned);
                aligned = String.format("%-" + width + "s", aligned);

                System.out.print(aligned);

            }
            System.out.println();
        }

    }

}
