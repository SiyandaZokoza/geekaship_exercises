/**
 * Created by CHARLIE on 2015-09-22.
 */
public class FindTheMaximum {
    public static void main(String[] args){
        int[] numbers = {3, 5, 7, 1, 9, 14, 4};
        int highest = -99;

        for (int i = 0; i < numbers.length; i++){
            highest = Math.max(highest, numbers[i]);
        }

        System.out.println("Highest = " + highest);
    }
}
