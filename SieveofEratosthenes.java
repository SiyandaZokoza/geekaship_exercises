import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-08.
 */
public class SieveofEratosthenes {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter any integer value: ");
        int value = scan.nextInt();

        Sieve(value);

    }

    public static void Sieve(int N){

        int[] primeArray = new int[N];
        // initially assume all integers are prime
        boolean[] isPrime = new boolean[N + 1];
        for (int i = 2; i <= N; i++) {
            isPrime[i] = true;

        }

        // mark non-primes <= N using Sieve of Eratosthenes
        for (int i = 2; i*i <= N; i++) {

            // if i is prime, then mark multiples of i as nonprime
            // suffices to consider multiples i, i+1, ..., N/i
            if (isPrime[i]) {

                for (int j = i; i*j <= N; j++) {
                    isPrime[i*j] = false;
                }
            }
        }
        for (int i = 2; i <= N; i++) {

            // if i is prime, then List prime numbers
            if (isPrime[i])
                System.out.println("Prime number: " + i);
        }

        // count primes
        int primeTotal = 0;
        for (int i = 2; i <= N; i++) {
            if (isPrime[i])
                primeTotal++;


        }
        System.out.print("The number of primes <= " + N + " is " + primeTotal);


    }



}
