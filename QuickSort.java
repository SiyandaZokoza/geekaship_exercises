/**
 * Created by CHARLIE on 2015-09-23.
 */
public class QuickSort {
    private int array[];
    private int length;
    public static void main(String[] args){
        QuickSort sorter = new QuickSort();
        int[] input = {2,45,20,46,15,25,8,9,5,12};

        System.out.println("Original Numbers");
        for (int i = 0; i < input.length; i++)
            System.out.print(input[i] + " ");

        sorter.sort(input);

        System.out.println("\n\nSorted Numbers");
        for(int i:input){
            System.out.print(i + " ");
        }
    }

    public void sort(int[] inputArray) {

        if (inputArray == null || inputArray.length == 0) {
            return;
        }
        this.array = inputArray;
        length = inputArray.length;
        quickSort(0, length - 1);
    }

    private void quickSort(int firstIndex, int lastIndex) {

        int left = firstIndex;
        int right = lastIndex;

        int pivot = array[firstIndex+(lastIndex-firstIndex)/2];

        while (left <= right) {

            while (array[left] < pivot) {
                left++;
            }
            while (array[right] > pivot) {
                right--;
            }
            if (left <= right) {
                exchangeNumbers(left, right);
                left++;
                right--;
            }
        }

        if (firstIndex < right)
            quickSort(firstIndex, right);
        if (left < lastIndex)
            quickSort(left, lastIndex);
    }

    private void exchangeNumbers(int left, int right) {
        int temp = array[left];
        array[left] = array[right];
        array[left] = temp;

    }
}
