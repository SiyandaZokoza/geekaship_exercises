/**
 * Created by CHARLIE on 2015-09-17.
 */
import java.util.Scanner;
public class BubbleSort {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int[] num = new int[5];
        int count = 1;
        for (int i = 0; i < 5; i++)
        {
            System.out.print("Please enter number: " + count + "");
            num[i] = in.nextInt();
            count++;
        }

        int j;
        boolean hasChanged = true;   // set flag to true to begin first pass
        int temp;   //holding variable

        while ( hasChanged )
        {
            hasChanged = false;    //set flag to false awaiting a possible swap
            for( j=0;  j < num.length -1;  j++ )
            {
                if ( num[ j ] < num[j+1] )   // change to > for ascending sort
                {
                    temp = num[ j ];                //swap elements
                    num[ j ] = num[ j+1 ];
                    num[ j+1 ] = temp;
                    hasChanged = true;              //shows a swap occurred
                }
            }
        }

        for (int i = 0; i < 5; i++)
            System.out.print(num[i] + "");

    }
}
