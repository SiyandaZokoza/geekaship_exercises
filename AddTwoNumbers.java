/**
 * Created by CHARLIE on 2015-09-17.
 */
import java.util.*;
import java.util.Scanner;
public class AddTwoNumbers {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int sum = 0;
        String userInput;

        System.out.print("Please enter 2 numbers and separate them by a space ");
        userInput = in.nextLine();

        String[] strings = userInput.split(" ");
        int[] numbers = new int[strings.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(strings[i].trim());
            sum += numbers[i];
        }

        System.out.println("The sum is " + sum);
    }
}




