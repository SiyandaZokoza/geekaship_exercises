/**
 * Created by CHARLIE on 2015-09-21.
 */
public class _12TimesTable {
    public static void main(String[] args){
        int answer, x = 1;

        do {
            for (int i = 1; i <= 12; i++){
                answer = x * i;
                System.out.println(x + " * " + i + " = " + answer);
            }
            System.out.println("...");
            x++;

        }while (x <= 12);

    }
}
