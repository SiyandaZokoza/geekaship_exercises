/**
 * Created by CHARLIE on 2015-09-17.
 */

public class BalancingBrackets {
    public static void main(String[] args){

        for(int i = 0; i <= 10; i+=2){
            String bracks = generate(i);
            if (checkBrackets(bracks) == false)
                System.out.println(bracks + "\t" + "NOT OK");
            else if (checkBrackets(bracks) == true && i == 0)
                System.out.println("(empty)" + "\t" + "OK");
            else if (checkBrackets(bracks) == true)
                System.out.println(bracks + "\t" + "OK");
        }


    }
    public static boolean checkBrackets(String str){

        int mismatchedBrackets = 0;
        for(char ch:str.toCharArray()){
            if(ch == '['){
                mismatchedBrackets++;
            }else if(ch == ']'){
                mismatchedBrackets--;
            }else{
                return false ; //non-bracket chars
            }
            if(mismatchedBrackets < 0){ //close bracket before open bracket
                return false;
            }
        }

        return mismatchedBrackets == 0;
    }

    public static String generate(int n){
        String ans = "";
        int openBracketsLeft = n;
        int unclosed = 0;
        while(ans.length() < n){
            if(Math.random() >= .5 && openBracketsLeft > 0 || unclosed == 0){
                ans += '[';
                openBracketsLeft--;
                unclosed++;
            }else{
                ans += ']';
                unclosed--;
            }
        }
        return ans;
    }

}
