import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-09-23.
 */
public class UserInput {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        String input;
        int number;

        System.out.print("Please input a string ");
        input = in.nextLine();

        System.out.print("Please enter the integer 75000 ");
        number = in.nextInt();

        System.out.println(input + " " + number);

    }
}
