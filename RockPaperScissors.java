import java.util.Random;
import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-09.
 */
public class RockPaperScissors {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        Random AI = new Random();
        int rockSum = 0, paperSum = 0, scissorSum = 0, computerSelection;
        String computerSelectionOutput, userSelection;

        for (int i = 0; i < 2; i++) {

            System.out.print("Please make a selection [Rock, Paper or Scissors]: ");
            userSelection = scan.nextLine();

            computerSelection = AI.nextInt(2);

            if (computerSelection == 0) {
                if (userSelection.equals("Rock")) {
                    rockSum++;
                    System.out.println("--- No winner found ---\nThe computer chose Rock");

                }
                if (userSelection.equals("Paper")) {
                    paperSum++;
                    System.out.println("--- You win ---\nThe computer chose Rock");
                }

                if (userSelection.equals("Scissors")) {
                    scissorSum++;
                    System.out.println("--- You lose ---\nThe computer chose Rock");

                }
            } else if (computerSelection == 1) {
                if (userSelection.equals("Rock")) {
                    rockSum++;
                    System.out.println("--- You lose ---\nThe computer chose Paper");

                }
                if (userSelection.equals("Paper")) {
                    paperSum++;
                    System.out.println("--- No winner found ---\nThe computer chose Paper");
                }

                if (userSelection.equals("Scissors")) {
                    scissorSum++;
                    System.out.println("--- You win ---\nThe computer chose Paper");

                }
            } else if (computerSelection == 2) {
                if (userSelection.equals("Rock")) {
                    rockSum++;
                    System.out.println("--- You win ---\nThe computer chose Scissors");

                }
                if (userSelection.equals("Paper")) {
                    paperSum++;
                    System.out.println("--- You lose ---\nThe computer chose Scissors");
                }

                if (userSelection.equals("Scissors")) {
                    scissorSum++;
                    System.out.println("--- No winner found ---\nThe computer chose Scissors");

                }
            }

            if (rockSum > 0)
                computerSelection = 0;
            else if (paperSum > 0)
                computerSelection = 1;
            else if (scissorSum > 0)
                computerSelection = 2;

        }
    }
}
