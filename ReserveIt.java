import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-09-23.
 */
public class ReserveIt {
    public static void main(String[] args){

        ReverseString();
        ReverseArray();

    }

    public static void ReverseString(){
        Scanner in = new Scanner(System.in);
        String original, reverse = "";
        System.out.print("Enter a string to reverse: ");
        original = in.nextLine();

        int length = original.length();

        for ( int i = length - 1 ; i >= 0 ; i-- )
            reverse = reverse + original.charAt(i);

        System.out.println("Reverse of entered string is: " + reverse);

    }

    public static void ReverseArray(){


        String[] array = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};

        System.out.println("\nOriginal Array: ");
        for (int x = 0; x < array.length; x++)
            System.out.println(array[x]);

        System.out.println("\nReversed Array: ");
        for(int i = array.length - 1; i >= 0;i--){
            System.out.println(array[i]);
        }
    }



}
