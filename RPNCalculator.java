import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-06.
 */
public class RPNCalculator {
    public static void main(String[] args){

        System.out.print("Enter an expression: ");
        Scanner scan = new Scanner(System.in);
        String expression = scan.nextLine();
        evalRPN(expression);

    }

    public static void evalRPN(String expr){
        String cleanExpr = cleanExpr(expr);
        LinkedList<Double> stack = new LinkedList<Double>();
        System.out.println("Input\tOperation\tStack after");
        for(String token:cleanExpr.split("\\s")){
            System.out.print(token+"\t");
            Double tokenNum = null;
            try{
                tokenNum = Double.parseDouble(token);
            }catch(NumberFormatException e){}
            if(tokenNum != null){
                System.out.print("Push\t\t");
                stack.push(Double.parseDouble(token+""));
            }else if(token.equals("*")){
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand * secondOperand);
            }else if(token.equals("/")){
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand / secondOperand);
            }else if(token.equals("-")){
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand - secondOperand);
            }else if(token.equals("+")){
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(firstOperand + secondOperand);
            }else if(token.equals("^")){
                System.out.print("Operate\t\t");
                double secondOperand = stack.pop();
                double firstOperand = stack.pop();
                stack.push(Math.pow(firstOperand, secondOperand));
            }else{//just in case
                System.out.println("Error");
                return;
            }
            System.out.println(stack);
        }
        System.out.println("\nFinal answer: " + stack.pop());
    }

    private static String cleanExpr(String expr){
        //remove all non-operators, non-whitespace, and non digit chars
        return expr.replaceAll("[^\\^\\*\\+\\-\\d/\\s]", "");
    }
}
