import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-01.
 */
public class MorseCode {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String input;

        String[] strings = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "0", " " };
        String[] morse = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
                "-.--", "--..", ".----", "..---", "...--", "....-", ".....",
                "-....", "--...", "---..", "----.", "-----", " " };

        System.out.print("Please enter the letter [S] to convert to Strings,\nOr enter the letter [M] to convert to Morse: ");
        input = in.nextLine();

        if (input.equals("M") || input.equals("m")){
            System.out.print("Please enter the text you would like to convert to Morse Code: ");
            String english = in.nextLine();

            char[] translates = (english.toLowerCase()).toCharArray();
            System.out.println(ConvertToMorse(translates, morse)); //calls on method ConvertToMorse
        }
        else if (input.equals("S") || input.equals("s")){
            System.out.print("Please enter the Morse Code you would like to convert to English (Separate each code letter by space): ");
            String code = in.nextLine();

            String[] translate = code.split(" ");
            for (int x = 0; x < translate.length; x++){

                for (int y = 0; y < strings.length; y++) {
                    if (translate[x].equals(morse[y]))
                        System.out.print(strings[y] + " ");

                }
            }

        }

        else
            System.out.println("Invalid input, please try again");

    }

    public static String ConvertToMorse(char[] translates, String[] morse){
        String morseCode = "";
        for (int j = 0; j < translates.length; j++)
        {
            char a = translates[j];
            if(Character.isLetter(a))
            {
                morseCode += morse[a - 'a'];
            }
        }
        return morseCode;
    }

}
