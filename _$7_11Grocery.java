import java.text.NumberFormat;

/**
 * Created by CHARLIE on 2015-09-21.
 */
public class _$7_11Grocery {
    public static void main(String[] args){
        double total=7.11, itemPrice;
        NumberFormat formatter = NumberFormat.getCurrencyInstance(); //currency format instance

        itemPrice = 1 + (1 / (total - 4));
        itemPrice = Math.round(itemPrice * 100)/100.0; //rounding off to 2 decimal places

        String moneyString = formatter.format(itemPrice); //setting the currency type to itemPrice

        System.out.println("The price of each item is: " + moneyString);

    }
}
