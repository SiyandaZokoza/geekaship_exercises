/**
 * Created by CHARLIE on 2015-09-22.
 */
import java.io.File;
public class DeleteAFile {
    public static void main(String[] args){
        try{
            File folder = new File("C:\\Documents\\Infoware Studios\\Practical Practice\\src\\docs");

            if(folder.delete()){
                System.out.println("the folder " + folder.getName() + " Was deleted!");
            }else{
                System.out.println("Folder Delete Operation Failed. Check: " + folder);
            }
        }catch(Exception e1){
            e1.printStackTrace();
        }

        try{
            File file = new File("C:\\Documents\\Infoware Studios\\Practical Practice\\src\\input.txt");

            if(file.delete()){
                System.out.println("the file " + file.getName() + " Was deleted!");
            }else{
                System.out.println("File Delete Operation Failed. Check: " + file);
            }
        }catch(Exception e1){
            e1.printStackTrace();
        }

    }
}
