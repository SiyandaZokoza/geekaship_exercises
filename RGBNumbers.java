import java.util.Scanner;
import java.lang.String;

/**
 * Created by CHARLIE on 2015-09-23.
 */
public class RGBNumbers {
    public static void main(String[] args) {

        int r, g, b;

        Scanner in = new Scanner(System.in);

        System.out.print("Please enter R: ");
        r = in.nextInt();

        System.out.print("Please enter G: ");
        g = in.nextInt();

        System.out.print("Please enter B: ");
        b = in.nextInt();

        String output = rgbToHex(r).toUpperCase()+rgbToHex(g).toUpperCase()+rgbToHex(b).toUpperCase();
        System.out.println("#" + output);


    }
    static String rgbToHex(int parameter){
        String hex = Integer.toHexString(parameter);
        return  hex;
    }



}
