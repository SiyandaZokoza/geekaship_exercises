import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-09-23.
 */
public class SumOfDigits {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        int digits;
        int sum = 0;

        System.out.print("Please enter digits: ");
        digits = in.nextInt();

        int input = digits;

        while (input != 0){
            int lastdigit = input % 10;
            sum += lastdigit;
            input /= 10;
        }

        System.out.println("The sum is " + sum);
    }
}
