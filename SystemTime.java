/**
 * Created by CHARLIE on 2015-09-23.
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
public class SystemTime{
    public static void main(String[] args){
        Calendar cal = Calendar.getInstance();
        System.out.println("System time in HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println( sdf.format(cal.getTime()) );
    }

}
