/**
 * Created by CHARLIE on 2015-09-21.
 */
import java.io.*;
public class AddFromFile {
    public static void main(String[] args){

        AddFromFileFunction();

    }

    public  static void AddFromFileFunction(){
        int sum = 0, i = 0;
        int[] numbers = new int[5];


        try{

            FileInputStream filestream = new FileInputStream("C:\\Documents\\Infoware Studios\\Practical Practice\\src\\File.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(filestream));
            String strLine;

            while ((strLine = br.readLine()) != null){
                try{

                    numbers[i] = Integer.valueOf(strLine);
                    System.out.print(numbers[i]);
                    sum += numbers[i];
                    if (i < 4)
                        System.out.print(" + ");
                    i++;
                }catch (NumberFormatException e){
                    System.err.println("Error: " + e.getMessage());
                }

            }

            filestream.close(); // close input stream
        }catch (Exception e){
            System.err.println("Error: " + e.getMessage());
        }

        System.out.print(" = " + sum);
    }

}

