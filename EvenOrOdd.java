import java.util.Scanner;
/**
 * Created by CHARLIE on 2015-09-22.
 */
public class EvenOrOdd {
    public static void main(String[] args){

        int userInput;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter any integer number: ");
        userInput = in.nextInt();

        if (userInput%2 == 0)
            System.out.println("The number " + userInput + " is an EVEN number");
        else if (userInput%2 == -1 || userInput%2 == 1)
            System.out.println("The number " + userInput + " is an ODD number");


        System.out.print("\nEnter another integer number: ");
        userInput = in.nextInt();

        if ((userInput & 1) == 1)
            System.out.println("The number " + userInput + " is an ODD number");
        else if ((userInput & 2) == 0)
            System.out.println("The number " + userInput + " is an EVEN number");


    }
}
