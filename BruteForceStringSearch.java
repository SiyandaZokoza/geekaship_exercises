import java.util.Scanner;

/**
 * Created by CHARLIE on 2015-10-09.
 */
public class BruteForceStringSearch {
    public static void main(String args[]){
        BruteForceStringSearch search = new BruteForceStringSearch();
        Scanner sc = new Scanner(System.in);
        String text = "This is the text that you can search";
        System.out.print("Enter the word you want to search: ");
        String pattern = sc.next();

        search.setString(text, pattern);
        int position = search.search();

        if(position != -1)
            System.out.println("The text: ["+ pattern + "] is found at position " + position);
        else
            System.out.print("The text: [" + pattern + "] was not found");
    }
    char[] text, pattern;
    int intText, intPattern;

    public void setString(String textString,String patternString){
        text = textString.toCharArray();
        pattern = patternString.toCharArray();
        intText = textString.length();
        intPattern = patternString.length();
    }
    public int search() {
        for (int position = 0; position < intText - intPattern; position++) {
            int direction =0;
            while (direction < intPattern && text[position+direction] == pattern[direction]) {
                direction++;
            }
            if (direction == intPattern) return position;
        }
        return -1;
    }
}
