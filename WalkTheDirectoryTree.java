import java.io.File;
import java.io.FileFilter;

/**
 * Created by CHARLIE on 2015-10-08.
 */
public class WalkTheDirectoryTree {
    public static void main(String[] args)
    {
        File file = new File("C:\\Documents\\Infoware Studios\\Practical Practice\\src");
        File[] fileList = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });

        if(fileList != null)
        {

            for(File f: fileList)
            {
                System.out.println("File in directory: " + f.getName());
            }
        }

    }
}
